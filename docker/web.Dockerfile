FROM node:16

WORKDIR /app

COPY . .

RUN mv .env.docker .env.local
RUN yarn install
RUN yarn run build

CMD yarn start
