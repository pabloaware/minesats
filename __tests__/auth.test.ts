import { createMocks } from "node-mocks-http";
import crypto from "crypto";
import secp256k1 from "secp256k1";
import { bech32 } from "bech32";
import _ from "lodash";

import db from "@/api/_db";
import signup from "@/api/signup";
import login from "@/api/login";
import { getAuthServerSideProps } from "@context/AuthContext";

import { genKeyPair } from "./util";

describe("/api/signup", () => {
  test("verifies secp256k1 signature", async () => {
    const k1 = crypto.randomBytes(32);
    const lnurl = "LNURL...";
    await db.insert({ k1: k1.toString("hex"), lnurl }).into("auth");

    // Generate two keypairs
    const [privKey1, pubKey1] = genKeyPair();
    const [privKey2, pubKey2] = genKeyPair();
    // Sign with first private key
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey1);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const { req, res } = createMocks({
      method: "GET",
      query: {
        k1: k1.toString("hex"),
        sig: Buffer.from(sig).toString("hex"),
        // But send second public key
        key: Buffer.from(pubKey2).toString("hex"),
      },
    });

    await signup(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "invalid sig",
    });
  });

  test("creates user session and deletes challenge", async () => {
    const k1 = crypto.randomBytes(32);
    const lnurl = "LNURL...";
    await db.insert({ k1: k1.toString("hex"), lnurl }).into("auth");

    // Generate two keypairs
    const [privKey, pubKey] = genKeyPair();
    // Sign with first private key
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const query = {
      k1: k1.toString("hex"),
      sig: Buffer.from(sig).toString("hex"),
      // But send second public key
      key: Buffer.from(pubKey).toString("hex"),
    };
    const { req, res } = createMocks({
      method: "GET",
      query,
    });

    await signup(req, res);

    expect(res.statusCode).toBe(200);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "OK",
    });

    const challenge = await db
      .select("*")
      .from("auth")
      .where({ k1: query.k1 })
      .first();
    expect(_.isEmpty(challenge)).toBeTruthy();
    const { session_id } = await db
      .select("session_id")
      .from("users")
      .where({ pubkey: query.key })
      .first();
    expect(session_id).toBeDefined();
  });

  test("responds with 400 Bad Request on empty query", async () => {
    const { req, res } = createMocks({
      method: "GET",
    });

    await signup(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "no k1 provided",
    });
  });

  test("responds with 400 Bad Request if k1 unknown", async () => {
    const k1 = crypto.randomBytes(32);

    const [privKey, pubKey] = genKeyPair();
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const { req, res } = createMocks({
      method: "GET",
      query: {
        k1: k1.toString("hex"),
        sig: Buffer.from(sig).toString("hex"),
        key: Buffer.from(pubKey).toString("hex"),
      },
    });

    await signup(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "unknown k1",
    });
  });

  test("responds with 400 Bad Request if challenge expired", async () => {
    const k1 = crypto.randomBytes(32);
    const lnurl = "LNURL...";
    await db
      .insert({
        k1: k1.toString("hex"),
        lnurl,
        created: db.raw(`to_timestamp(?)`, [+Date.now() - 1000 * 60 * 60]),
      })
      .into("auth");

    const [privKey, pubKey] = genKeyPair();
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const { req, res } = createMocks({
      method: "GET",
      query: {
        k1: k1.toString("hex"),
        sig: Buffer.from(sig).toString("hex"),
        key: Buffer.from(pubKey).toString("hex"),
      },
    });

    await signup(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "k1 expired",
    });
  });

  test("responds with 400 Bad Request if pubkey already exists", async () => {
    const k1 = crypto.randomBytes(32);
    const lnurl = "LNURL...";
    await db
      .insert({
        k1: k1.toString("hex"),
        lnurl,
      })
      .into("auth");

    const [privKey, pubKey] = genKeyPair();
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const key = Buffer.from(pubKey).toString("hex");
    await db.insert({ pubkey: key }).into("users");

    const { req, res } = createMocks({
      method: "GET",
      query: {
        k1: k1.toString("hex"),
        sig: Buffer.from(sig).toString("hex"),
        key,
      },
    });

    await signup(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "pubkey already exists",
    });
  });
});

describe("/pages/signup", () => {
  test("returns LNURL-auth with signup url as callback", async () => {
    const { req, res } = createMocks();

    const ctx = { req, res };
    const { props } = await getAuthServerSideProps("signup")(ctx);

    expect(props).toEqual({
      k1: expect.any(String),
      lnurl: expect.any(String),
    });

    const { lnurl } = props;
    const { prefix, words } = bech32.decode(lnurl, 1023);
    expect(prefix).toEqual("lnurl");
    const decoded = Buffer.from(bech32.fromWords(words)).toString("utf8");
    expect(decoded).toMatch(
      /^https:\/\/test\.minesats\.gg\/api\/signup\?tag=login&k1=[0-9a-f]+&action=register$/
    );
  });

  test("responds with Set-Cookie header", async () => {
    const { req, res } = createMocks();

    const ctx = { req, res };
    const {
      props: { k1 },
    } = await getAuthServerSideProps("signup")(ctx);

    const cookie = ctx.res.getHeader("Set-Cookie");
    const { session_id } = await db
      .select("session_id")
      .from("auth")
      .where({ k1 })
      .first();
    expect(cookie).toMatch(
      new RegExp(
        `^session=${session_id}; Max-Age=${
          60 * 60 * 24 * 365
        }; Secure; HttpOnly; Path=/$`
      )
    );
  });
});

describe("/api/login", () => {
  test("verifies secp256k1 signature", async () => {
    const k1 = crypto.randomBytes(32);
    const lnurl = "LNURL...";
    await db.insert({ k1: k1.toString("hex"), lnurl }).into("auth");

    // Generate two keypairs
    const [privKey1, pubKey1] = genKeyPair();
    const [privKey2, pubKey2] = genKeyPair();
    // Sign with first private key
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey1);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const { req, res } = createMocks({
      method: "GET",
      query: {
        k1: k1.toString("hex"),
        sig: Buffer.from(sig).toString("hex"),
        // But send second public key
        key: Buffer.from(pubKey2).toString("hex"),
      },
    });

    await login(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "invalid sig",
    });
  });

  test("creates user session and deletes challenge", async () => {
    const k1 = crypto.randomBytes(32);
    const lnurl = "LNURL...";
    await db.insert({ k1: k1.toString("hex"), lnurl }).into("auth");

    // Generate two keypairs
    const [privKey, pubKey] = genKeyPair();
    // Sign with first private key
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const key = Buffer.from(pubKey).toString("hex");
    await db.insert({ pubkey: key }).into("users");

    const query = {
      k1: k1.toString("hex"),
      sig: Buffer.from(sig).toString("hex"),
      // But send second public key
      key: Buffer.from(pubKey).toString("hex"),
    };
    const { req, res } = createMocks({
      method: "GET",
      query,
    });

    await login(req, res);

    expect(res.statusCode).toBe(200);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "OK",
    });

    const challenge = await db
      .select("*")
      .from("auth")
      .where({ k1: query.k1 })
      .first();
    expect(_.isEmpty(challenge)).toBeTruthy();
    const { session_id } = await db
      .select("session_id")
      .from("users")
      .where({ pubkey: query.key })
      .first();
    expect(session_id).toBeDefined();
  });

  test("responds with 400 Bad Request on empty query", async () => {
    const { req, res } = createMocks({
      method: "GET",
    });

    await login(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "no k1 provided",
    });
  });

  test("responds with 400 Bad Request if k1 unknown", async () => {
    const k1 = crypto.randomBytes(32);

    const [privKey, pubKey] = genKeyPair();
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const { req, res } = createMocks({
      method: "GET",
      query: {
        k1: k1.toString("hex"),
        sig: Buffer.from(sig).toString("hex"),
        key: Buffer.from(pubKey).toString("hex"),
      },
    });

    await login(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "unknown k1",
    });
  });

  test("responds with 400 Bad Request if challenge expired", async () => {
    const k1 = crypto.randomBytes(32);
    const lnurl = "LNURL...";
    await db
      .insert({
        k1: k1.toString("hex"),
        lnurl,
        created: db.raw(`to_timestamp(?)`, [+Date.now() - 1000 * 60 * 60]),
      })
      .into("auth");

    const [privKey, pubKey] = genKeyPair();
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const { req, res } = createMocks({
      method: "GET",
      query: {
        k1: k1.toString("hex"),
        sig: Buffer.from(sig).toString("hex"),
        key: Buffer.from(pubKey).toString("hex"),
      },
    });

    await login(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "k1 expired",
    });
  });

  test("responds with 400 Bad Request if pubkey not exists", async () => {
    const k1 = crypto.randomBytes(32);
    const lnurl = "LNURL...";
    await db
      .insert({
        k1: k1.toString("hex"),
        lnurl,
      })
      .into("auth");

    const [privKey, pubKey] = genKeyPair();
    const sigObj = secp256k1.ecdsaSign(Uint8Array.from(k1), privKey);
    const sig: Uint8Array = secp256k1.signatureExport(sigObj.signature);

    const { req, res } = createMocks({
      method: "GET",
      query: {
        k1: k1.toString("hex"),
        sig: Buffer.from(sig).toString("hex"),
        key: Buffer.from(pubKey).toString("hex"),
      },
    });

    await login(req, res);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      status: "ERROR",
      reason: "unknown pubkey",
    });
  });
});

describe("/pages/login", () => {
  test("returns LNURL-auth with login url as callback", async () => {
    const { req, res } = createMocks();

    const ctx = { req, res };
    const { props } = await getAuthServerSideProps("login")(ctx);

    expect(props).toEqual({
      k1: expect.any(String),
      lnurl: expect.any(String),
    });

    const { lnurl } = props;
    const { prefix, words } = bech32.decode(lnurl, 1023);
    expect(prefix).toEqual("lnurl");
    const decoded = Buffer.from(bech32.fromWords(words)).toString("utf8");
    expect(decoded).toMatch(
      /^https:\/\/test\.minesats\.gg\/api\/login\?tag=login&k1=[0-9a-f]+&action=login$/
    );
  });

  test("responds with Set-Cookie header", async () => {
    const { req, res } = createMocks();

    const ctx = { req, res };
    const {
      props: { k1 },
    } = await getAuthServerSideProps("login")(ctx);

    const cookie = ctx.res.getHeader("Set-Cookie");
    const { session_id } = await db
      .select("session_id")
      .from("auth")
      .where({ k1 })
      .first();
    expect(cookie).toMatch(
      new RegExp(
        `^session=${session_id}; Max-Age=${
          60 * 60 * 24 * 365
        }; Secure; HttpOnly; Path=/$`
      )
    );
  });
});
