import crypto from "crypto";
import { createMocks } from "node-mocks-http";
import _ from "lodash";

import db from "@/api/_db";
import user from "@/api/user";

import { genKeyPair } from "./util";

describe("/api/user", () => {
  it("checks for active sessions", async () => {
    const [__, rawPubkey] = genKeyPair();
    const pubkey = Buffer.from(rawPubkey).toString("hex");
    const session_id = "OGM0NzMxYTItYTQ0Yy00NzM0LTkxNzgtOTI2MzM5N2Q3Njdm";
    await db.insert({ pubkey, session_id }).into("users");

    const { req, res } = createMocks({
      method: "GET",
      cookies: {
        session: session_id,
      },
    });

    await user(req, res);

    expect(res.statusCode).toBe(200);
    expect(res._isJSON()).toBeTruthy();
    expect(res._getJSONData()).toEqual({
      display_name: pubkey,
      pubkey,
      last_logout: null,
    });
  });
});
