# minesats

Inofficial player stats and more for [Satlantis](https://satlantis.net/).

![](./img/2023-03-04-113759_1920x1080_scrot.png)

Website: minesats.gg

## Development

This project consists of two parts:

- bot in golang
- website using NextJS

### Bot setup

1. Start postgres container with initial credentials given in `docker-compose.yml`:

```txt
$ docker-compose up -d db
```

2. `cd` into `bot/` and copy `.env.template` to `.env` and configure `BOT_TOKEN` to use your discord token.
   This can also be your user token if you have no bot in the server but keep in mind that "self-bots" are [against Discord ToS](https://support.discord.com/hc/en-us/articles/115002192352-Automated-user-accounts-self-bots-).
   You should be able to find your user token in the network requests made while logged into Discord.
   You can also read [this article](https://www.androidauthority.com/get-discord-token-3149920/) if you have trouble finding it. Here's the short answer from that article:

   > To get your Discord token, log into Discord on a browser. Open Developer Tools, then click Network. Press F5 on your keyboard to reload the page. Type /api into the Filter field, then click library. Click the Headers tab, then scroll down to authorization to find your Discord token.

3. Join Satlantis discord if you haven't yet since you will need to see the `#live-action` channel: https://discord.com/invite/CqyGY339xp
4. Run the bot with `go run .`. It should start to fetch all messages from the beginning of the channel, insert them into the database and parse them.

### Website setup

Install app dependencies and run website. Database must be up.

```txt
$ yarn install
$ yarn run dev
```

Since HTTPS is required for LNURL-auth, login/signup won't work out of the box locally.
However, you can use [`ngrok`](https://ngrok.com/) for that. After you have installed it, run `ngrok http 3000` to forward all requests to your local website running at port 3000. Then you just need to set `NEXT_PUBLIC_BASE_URL` in `.env.local` to the ngrok URL.
You can use the script in `scripts/run_with_ngrok.sh` to automate these steps.
