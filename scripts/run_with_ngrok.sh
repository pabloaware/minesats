#!/usr/bin/env bash

set -e

usage() {
  echo "Usage: $(basename $0) [<PORT>]"
  echo
  echo "Run website with NEXT_PUBLIC_BASE_URL set to ngrok url to use HTTPS locally for LNURL-auth usage."
  echo "Default port is 3000."
}

if [[ $# -gt 1 ]]; then
  usage
  exit 1
fi

PORT="3000"
if [ ! -z $1 ]; then
  PORT="$1"
fi

ngrok http "$PORT" --log=stdout > ngrok.log &
NGROK_URL=
while [ -z "$NGROK_URL" ]; do
  NGROK_URL="$(curl http://127.0.0.1:4040/api/tunnels --silent | jq -r '.tunnels[0].public_url // empty' | sed -e 's/^https:\/\///')"
  sleep 0.1s
done
sed -i -e "s|NEXT_PUBLIC_BASE_URL=.*|NEXT_PUBLIC_BASE_URL=$NGROK_URL|" .env.local
echo "Ngrok forwarding requests from $NGROK_URL to localhost:$PORT"
yarn run dev -p "$PORT"
