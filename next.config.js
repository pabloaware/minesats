const { withPlausibleProxy } = require("next-plausible");

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/i,
      issuer: /\.[jt]sx?$/,
      use: ["@svgr/webpack"],
    });

    // Fixes npm packages that depend on `fs` module
    config.resolve.fallback = { fs: false };

    return config;
  },
};

module.exports = withPlausibleProxy()(nextConfig);
