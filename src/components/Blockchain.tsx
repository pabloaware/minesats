import { useBlockRewards } from "@context/BlockRewardContext";
import { useColor } from "@context/ColorContext";
import { useNextBlockTimer } from "@context/NextBlockTimerContext";

export default function Blockchain() {
  const { val: blockRewards } = useBlockRewards();
  const nextBlockTimer = useNextBlockTimer();
  const pickColor = useColor();
  const now = +new Date();
  const timeFormatter = new Intl.RelativeTimeFormat(`en`, { style: `narrow` });
  const satsFormatter = new Intl.NumberFormat(`en`);

  return (
    <div className="flex flex-row">
      <div className="flex flex-col rounded bg-neutral-400 w-[128px] h-[128px] mx-1 p-1 justify-center items-center text-center break-all text-xs">
        <div className="py-1 font-bold">Next Block:</div>
        <div className="py-1">
          {timeFormatter.format(Math.floor(nextBlockTimer / 1000), "seconds")}
        </div>
      </div>
      {blockRewards.slice(0, 10).map((block, i) => {
        const color = pickColor(block.name);
        return (
          <div
            style={{ border: 7, borderColor: color, borderStyle: "solid" }}
            className="flex flex-col rounded bg-neutral-500 w-[128px] h-[128px] mx-1 p-1 justify-center items-center text-center break-all text-xs"
            key={i}
          >
            <div className="py-1 font-bold">{block.name}</div>
            <div className="py-1">⚡{satsFormatter.format(block.amount)}</div>
            <div className="py-1">
              {timeFormatter.format(
                Math.floor((block.ts - now) / 60000),
                "minute"
              )}
            </div>
          </div>
        );
      })}
    </div>
  );
}
