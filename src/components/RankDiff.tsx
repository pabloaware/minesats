import SortDown from "@svg/sort-down.svg";
import SortUp from "@svg/sort-up.svg";
import SortNeutral from "@svg/sort-neutral.svg";

type RankDiffProps = {
  value: number;
};

export default function RankDiff({ value }: RankDiffProps) {
  const size = 16;
  const fill =
    value > 0
      ? "var(--color-green-400)"
      : value < 0
      ? "var(--color-red-400)"
      : "white";
  return (
    <div className="flex items-center">
      {value > 0 ? (
        <SortUp width={size} height={size} fill={fill} />
      ) : value < 0 ? (
        <SortDown width={size} height={size} fill={fill} />
      ) : (
        <SortNeutral width={size} height={size} fill={fill} />
      )}
      {Math.abs(value)}
    </div>
  );
}
