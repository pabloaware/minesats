import SortDown from "@svg/sort-down.svg";
import SortUp from "@svg/sort-up.svg";

type SortArrowProps = {
  order: "asc" | "desc";
  active: boolean;
};

export default function SortArrow({ order, active }: SortArrowProps) {
  const fill = active ? "white" : "var(--color-neutral-400)";
  const className = active ? "" : "hover:fill-neutral-300";
  return order === "desc" ? (
    <SortDown width={24} height={24} fill={fill} className={className} />
  ) : (
    <SortUp width={24} height={24} fill={fill} className={className} />
  );
}
