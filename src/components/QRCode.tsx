import { useRef, useEffect } from "react";
import qrcode from "qrcode";

import CopyText from "./CopyText";

type QRCodeProps = { data: string };
export default function QRCode({ data }: QRCodeProps) {
  const ref = useRef();
  useEffect(() => {
    qrcode.toCanvas(ref.current, data, console.error);
  }, [data]);

  return (
    <>
      <canvas ref={ref} className="my-3 m-auto" />
      <div className="flex flex-row m-auto">
        <CopyText text={data} />
      </div>
    </>
  );
}
