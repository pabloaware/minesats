import _ from "lodash";

import Page from "@components/Page";
import db from "@/api/_db";
import { Bar } from "react-chartjs-2";

export async function getServerSideProps(ctx) {
  const { name } = ctx.query;

  const rewards = await db
    .select([
      "miners.name",
      db.raw(`date_trunc('day', m.ts)::text AS ts`),
      db.raw(`SUM(amount) AS amount`),
    ])
    .from("miners")
    .leftJoin("blockrewards AS r", "r.name", "miners.name")
    .leftJoin("messages AS m", "m.id", "r.id")
    .where("miners.name", name)
    .groupBy(["miners.name", db.raw(`date_trunc('day', m.ts)`)])
    .orderBy("ts", "asc");

  return {
    props: {
      rewards,
    },
  };
}

type UserStatsProps = {
  name: string;
  rewards: { name: string; ts: string; amount: string }[];
};

export default function UserStats(props: UserStatsProps) {
  const labels = _.chain(props.rewards)
    .map("ts")
    .map((date) => date.split(" ")[0])
    .value();

  const options = {
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: true,
        text: "Rewards",
        color: "white",
      },
    },
    scales: {
      y: {
        title: {
          display: true,
          text: "sats",
          color: "white",
        },
        grid: {
          color: "white",
          tickColor: "white",
        },
        ticks: {
          color: "white",
        },
      },
      x: {
        title: {
          display: true,
          color: "white",
        },
        grid: {
          display: false,
        },
        ticks: {
          color: "white",
        },
      },
    },
  };
  const data = {
    labels,
    datasets: [
      {
        data: _.map(props.rewards, "amount"),
        backgroundColor: "#fdd62c", // var(--lightning-yellow)
      },
    ],
  };

  return (
    <Page marginAuto={false}>
      <div className="pr-3">
        <Bar options={options} data={data} />
      </div>
    </Page>
  );
}
