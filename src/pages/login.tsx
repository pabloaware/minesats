import Page from "@components/Page";
import QRCode from "@components/QRCode";
import { AuthChecker, getAuthServerSideProps } from "@context/AuthContext";

export const getServerSideProps = getAuthServerSideProps("login");

export default function Login({ k1, lnurl }) {
  return (
    <Page>
      <AuthChecker k1={k1}>
        <>
          <div className="text-center">Login with Lightning</div>
          <QRCode data={lnurl} />
        </>
      </AuthChecker>
    </Page>
  );
}
