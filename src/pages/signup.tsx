import Page from "@components/Page";
import QRCode from "@components/QRCode";
import { AuthChecker, getAuthServerSideProps } from "@context/AuthContext";

export const getServerSideProps = getAuthServerSideProps("signup");

export default function SignUp({ k1, lnurl }) {
  return (
    <Page>
      <AuthChecker k1={k1}>
        <>
          <div className="text-center">Sign up with Lightning</div>
          <QRCode data={lnurl} />
        </>
      </AuthChecker>
    </Page>
  );
}
