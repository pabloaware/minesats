import { NextApiRequest, NextApiResponse } from "next";

import db from "@/api/_db";
import { JSONResponse } from "@/api/_util";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<JSONResponse>
) {
  const session = req.cookies["session"];

  if (!session)
    return res.status(404).json({
      message: "session not found",
    });

  await db
    .from("users")
    .update({ session_id: null })
    .where("session_id", session);

  return res.status(200).json({
    message: "logout successful",
  });
}
