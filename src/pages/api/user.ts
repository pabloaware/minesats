import _ from "lodash";
import { NextApiRequest, NextApiResponse } from "next";

import db from "@/api/_db";
import { JSONResponse } from "@/api/_util";
import { Knex } from "knex";

export type User = {
  display_name: string;
  pubkey: string;
  last_logout?: string;
};

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

const handleSSE = async (req, res, session) => {
  res.writeHead(200, {
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    Connection: "keep-alive",
    // Required header to pass server-side events through nginx by disabling buffer
    // https://serverfault.com/questions/801628/for-server-sent-events-sse-what-nginx-proxy-configuration-is-appropriate
    "X-Accel-Buffering": "no",
  });
  let i = 0;
  let cancelled = false;
  res.on("close", (err) => {
    console.log(`session=${session} msg=client closed event stream`);
    cancelled = true;
  });
  console.log(`session=${session} msg=starting new event stream`);
  while (true) {
    if (cancelled) {
      return;
    }
    console.log(`session=${session} i=${i} msg=polling for session`);
    const row = await fetchUser(session);
    if (!_.isEmpty(row)) {
      console.log(`session=${session} i=${i} msg=session found!`);
      res.write(`data: ${JSON.stringify(row)}\n\n`);
      res.flush();
      return;
    }
    i++;
    if (i % 30 === 0) {
      // send ping message to keep connection alive
      res.write(`:ping\n\n`);
      res.flush();
    }
    await sleep(1000);
  }
};

const fetchUser = (session) =>
  db
    .select([
      db.raw(`COALESCE(player_name, pubkey) AS display_name`),
      "pubkey",
      "last_logout",
    ])
    .from("users")
    .leftJoin("players", "players.name", "users.player_name")
    .where("session_id", session)
    .first();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<JSONResponse>
) {
  const { sse } = req.query;
  const session = req.cookies["session"];

  if (!session)
    return res.status(400).json({
      message: "session cookie must be provided",
    });

  if (sse) {
    return handleSSE(req, res, session);
  }
  // check for session
  const row = await fetchUser(session);
  if (!_.isEmpty(row)) {
    return res.json(row);
  }

  return res.status(404).json({ message: "session not found" });
}
