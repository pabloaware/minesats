// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import _ from "lodash";

import conn from "@/api/_db";

export type TopMiner = {
  rank: number;
  prevRank?: number;
  name: string;
  amountTotal?: number;
  amount4320?: number;
  prevAmount4320?: number;
  delta4320?: number;
  amount1008?: number;
  prevAmount1008?: number;
  delta1008?: number;
  amount144?: number;
  prevAmount144?: number;
  delta144?: number;
};

const isSortKey = (sortKey: string): sortKey is TopMinerSortKey => {
  return ["amountTotal", "amount144", "amount1008", "amount4320"].includes(
    sortKey
  );
};

const isSortOrder = (order: string): order is SortOrder => {
  return ["asc", "desc"].includes(order);
};

export type TopMinerSortKey = keyof Omit<
  TopMiner,
  "rank" | "prevRank" | "name"
>;

type SortOrder = "asc" | "desc";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<TopMiner[]>
) {
  const sortKey = (req.query.sort as string) || "amountTotal";
  const sortOrder = (req.query.order as string) || "desc";
  if (!isSortKey(sortKey) || !isSortOrder(sortOrder)) {
    return res.status(400).end();
  }

  const rows = await conn
    .select([
      "*",
      // Can't use binding here since ORDER BY does not support it for ASC | DESC
      conn.raw(
        `ROW_NUMBER() OVER (ORDER BY ?? ${sortOrder}, name ASC) AS rank`,
        [_.snakeCase(sortKey)]
      ),
      sortKey !== "amountTotal"
        ? conn.raw(
            `ROW_NUMBER() OVER (ORDER BY ?? ${sortOrder}, name ASC) AS prev_rank`,
            [_.snakeCase(`prev_${sortKey}`)]
          )
        : conn.raw(`-1 AS prev_rank`),
    ])
    .from("top_miners")
    .orderBy(_.snakeCase(sortKey), sortOrder);
  res
    .status(200)
    .json(
      _.map(rows, (row) => _.mapKeys(row, (v, k) => _.camelCase(k)) as TopMiner)
    );
}
