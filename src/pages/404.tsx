import React from "react";

import Page from "@components/Page";

export default function NotFound() {
  return (
    <Page>
<pre>
      ⠀⣠⡤⡀<br/>
⠠⣇⣠⡧⠖⠒⠒⠛⠛⠒⠒⠒⠢⢄⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⣀<br/>
⠀⠀⡦⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣇⣀⣀⣀⠤⠴⠂⠉⠀⠀⠀⢹<br/>
⠀⠀⠇⡏⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇<br/>
⠀⠀⢸⢠⢠⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⠁<br/>
⠀⠀⢸⢸⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⡀<br/>
⠀⠀⠀⣦⡿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⣄<br/>
⠀⠀⠀⣇⡇⣀⡤⠤⠤⠖⠲⡖⠢⢄⡔⠀⠀⠀⠀⠀⠀⠀⣀⠤⠒⠒⠙<br/>
⠀⠀⠀⡇⢸⠀⠀⠀⠀⠀⠈⣇⠊⠁⠀⠀⠀⠀⠀⣀⠔⠋<br/>
⠀⠀⠀⡇⢸⠀⠀⠀⠀⠀⠀⠳⣀⠀⢀⣀⣀⠴⠊<br/>
⠀⠀⠀⡇⢸<br/>
⠀⠀⠀⡇⢸<br/>
      <br/>
You found flag 404!
</pre>
    </Page>
  );
}
