import React from "react";

import TopMiners from "@components/TopMiners";
import Page from "@components/Page";

export default function Home() {
  return (
    <Page>
      <TopMiners />
    </Page>
  );
}
