import { createContext, useContext, useState, useEffect } from "react";

import { useBlockRewards } from "@context/BlockRewardContext";

const NextBlockTimerContext = createContext<number>(-1);

export const NextBlockTimerProvider = ({ children }) => {
  const { val: blockRewards, update } = useBlockRewards();
  const [now, setNow] = useState<number>(+new Date());

  useEffect(() => {
    const interval = setInterval(() => {
      setNow(+new Date());
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  const lastBlock = blockRewards[0]?.ts ?? now;
  // next block 10 minutes after last block
  const nextBlock = lastBlock + 10 * 60 * 1000;
  const tDelta = nextBlock - now;
  if (tDelta < 0) {
    // console.log("updating blocks ...");
    // update();
  }

  return (
    <NextBlockTimerContext.Provider value={tDelta}>
      {children}
    </NextBlockTimerContext.Provider>
  );
};

export const useNextBlockTimer = () => useContext(NextBlockTimerContext);

export default NextBlockTimerProvider;
