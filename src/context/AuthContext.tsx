import { useRouter } from "next/router";
import React, { useState, createContext, useContext, useEffect } from "react";
import crypto from "crypto";
import { bech32 } from "bech32";

import db from "@/api/_db";
import { User } from "@/api/user";
import { AuthType } from "@/api/_auth";

const AuthContext = createContext<{
  user?: User;
  setUser: React.Dispatch<React.SetStateAction<User>>;
  loading: boolean;
}>(null);

export const AuthContextProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState<User>(null);

  useEffect(() => {
    // Check for active session
    fetch(`/api/user`)
      .then((r) => r.json())
      .then((user?: User) => {
        if (user?.pubkey) {
          setUser(user);
        }
      })
      .catch(console.error)
      .finally(() => setLoading(false));
  }, []);

  return (
    <AuthContext.Provider value={{ user, setUser, loading }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);

export default AuthContextProvider;

type AuthCheckerProps = { k1: string; children: JSX.Element };
export const AuthChecker = ({ k1, children }: AuthCheckerProps) => {
  const auth = useAuth();
  const router = useRouter();

  const pollUser = () => {
    const interval = setInterval(
      () =>
        fetch(`/api/user?k1=${k1}`)
          .then((r) => r.json())
          .then((user?: User) => {
            if (user?.pubkey) {
              auth.setUser(user);
              router.push("/");
            }
          })
          .catch(console.error),
      1000
    );
    return () => clearInterval(interval);
  };

  const sseUser = () => {
    const src = new EventSource(`/api/user?sse=1`);
    src.onmessage = (e) => {
      console.log(`Received data from event source: ${e.data}`);
      const user = JSON.parse(e.data);
      if (user?.pubkey) {
        auth.setUser(user);
        router.push("/");
      }
    };
    src.onopen = (e) => {
      console.log("Opened connection to event source");
    };
    src.onerror = (e) => {
      console.log("Error during connection to event source");
      src.close();
    };
    return () => {
      console.log("Closed connection to event source");
      src.close();
    };
  };

  useEffect(() => {
    if ("EventSource" in window) {
      return sseUser();
    }
    return pollUser();
  }, [k1, auth, router]);

  return children;
};

const maxAge = 60 * 60 * 24 * 365;

export const getAuthServerSideProps = (type: AuthType) => async (ctx) => {
  // Create new unauthenticated session and set cookie
  const k1 = crypto.randomBytes(32).toString("hex");

  const url = `https://${
    process.env.NEXT_PUBLIC_BASE_URL
  }/api/${type}?tag=login&k1=${k1}&action=${
    type === "signup" ? "register" : "login"
  }`;

  const words = bech32.toWords(Buffer.from(url, "utf8"));
  const lnurl = bech32.encode("lnurl", words, 1023);
  const [{ session_id }] = await db
    .insert({ k1, lnurl })
    .into("auth")
    .returning("session_id");

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie
  ctx.res.setHeader(
    "Set-Cookie",
    `session=${session_id}; Max-Age=${maxAge}; Secure; HttpOnly; Path=/`
  );

  return {
    props: {
      k1,
      lnurl,
    },
  };
};
